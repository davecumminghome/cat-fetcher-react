import React, {Fragment} from 'react';
import {
  SafeAreaView,
  View,
  Text,
  Image,
  StatusBar,
  Button,
  
} from 'react-native';

export default class App extends React.Component {

  _fetchCat = () => {
    return  fetch ("https://api.thecatapi.com/v1/images/search")
    .then(response => response.json())
    .then((responseJson) => {
      this.setState({
        catUrl : responseJson[0].url
      })
      return responseJson.cat
    })
    .catch((error) => {
    });

  }

  constructor(props){
    super(props)

    this.state = {
      catUrl: ""
    }
  }

  render() {
    const catImage = this.state.catUrl !== "" ? 
    <Image style={{width: 300, height: 300, marginTop:30}}
     source={{uri: this.state.catUrl}}/>: <View/>

    return (<Fragment>
      <StatusBar barStyle="dark-content" />
      <SafeAreaView style={{justifyContent:"center", alignItems:"center"}}>
          <Button  onPress={this._fetchCat}
          title="Fetch cat"
          />
          {catImage}
          <Text >{this.state.catUrl}</Text>
      </SafeAreaView>
    </Fragment>
    )
}};




